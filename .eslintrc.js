module.exports = {
    plugins: ['react'],
    extends: "react-app",
    rules: {
        'semi': ['error', 'never'],
        'padded-blocks': ['error', { 'classes': 'always' }],
        'space-before-function-paren': ['error', 'always'],
        'no-unused-vars': [2, { vars: 'local' }],
        'max-len': ['error', { code: 120 }],
        'indent': ['error', 2],
        'jsx-quotes': [2, 'prefer-single'],
        'prefer-destructuring': ['error', {
            'array': false,
            'object': true
        }],
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        'comma-dangle': ['error', 'never'],
        'class-methods-use-this': ['error', {
            exceptMethods: [
                'render',
                'getInitialState',
                'getDefaultProps',
                'getChildContext',
                'componentWillMount',
                'componentDidMount',
                'componentWillReceiveProps',
                'shouldComponentUpdate',
                'componentWillUpdate',
                'componentDidUpdate',
                'componentWillUnmount',
            ],
        }]
    }
}
