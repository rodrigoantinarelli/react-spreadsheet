# React Spreadsheet

This project is a step of the application process of GovPredict.

# Instructions

Here you will see some basic information to run the project.

## Installation

Go into the folder and Install the dependencies by running `yarn install` or just `yarn`:

```
cd react-spreadsheet && yarn
```

## Running

There's a script to run the project in development mode. In the terminal, run the command:

```
yarn run start
```

## Building

There's also a script to build the project. In the terminal, run the command:

```
yarn run build
```

A new `build` folder will be created and that's all. You can send this folder to your own server.

# Dev Features

* ES6
* React
* Mobx (Model and View store)
* Styled Components
* Linter
* Ramda
* React Router
* Validate.js

# Project Features

- Auto save in every 10 seconds
- List all your spreadsheets
- Create multiple spreadsheets
- Edit everything and save in LocalStorage
- Add rows and columns
- 404 Page
- Customized blank Home Page with a Headline call to create the first spreasheet
- Redirect if access to an invalid spreadsheet
- Validate fields

# To do

List of things that I would do to make this project better.

- Select column type (dropdown)
- Allow to add more than 26 columns (currently it goes from A to Z)
- Remove lines and/or columns
- UI Feedback when saving
- Remove spreadsheet
- Create tests using Jest
- Implement flow for type checker

---

Author: Rodrigo Antinarelli

Email: rodrigoantinarelli@gmail.com

Github: https://github.com/rodrigolabs
