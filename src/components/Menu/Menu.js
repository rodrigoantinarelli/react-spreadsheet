import React, { Component } from 'react'
import { Images } from '@/resources/images'
import { Link } from 'react-router-dom'
import { MenuWrapper } from './style'

class Menu extends Component {

  render () {
    return (
      <MenuWrapper>
        <Link to='/'>
          <img src={Images.logo} alt='Logo GovPredict' />
        </Link>
      </MenuWrapper>
    )
  }

}

export default Menu
