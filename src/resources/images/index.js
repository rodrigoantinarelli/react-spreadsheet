const Images = {
  logo: require('./logo-govpredict.svg'),
  icon: require('./spreadsheet-icon.png')
}

export {
  Images
}
